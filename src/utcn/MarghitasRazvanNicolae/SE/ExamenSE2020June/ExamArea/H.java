package utcn.MarghitasRazvanNicolae.SE.ExamenSE2020June.ExamArea;

public class H {

    public float n;
    public void f(int g){
        g=(int)n;
        System.out.println(g);
    }
}

class I extends H{

    private long t;
    // nu sunt sigur daca referinta se poate si asa si daca nu cumva deja intra in agregare
    I(long t/*,K o*/){
        this.t = t;
        System.out.println(t/*+o.key*/);
    }

    @Override
    public void f(int g) {
        super.f(g);

    }

    // nu era pe diagram dar e facuta pentru referinta la K
    public void a(K o){
        System.out.println(o.key);
    }


}


class J{

    public void i(I i)
    {
        System.out.println(i.n);
    }
}

class K{
    int key;

    M m;
    L l;
    K(M m){
        l=new L();
        this.m=m;
    }
}

class L{

    public void metA(){
        System.out.println("a");
    }
}

class M{
    public void metB(){
        System.out.println("b");
    }
}


